import React, { useState } from 'react';
import CounterInput from './components/CounterInput';
import CounterList from './components/CounterList';

// interface IPerson {
//   name: string,
//   age: number
// }
const App: React.FC = () => {

  // const sum = (num1: number, num2: number): void => {
  //   // return num1 + num2;
  //   // return (num1 + num2).toString();
  //   console.log(num1);
  //   console.log(num1 + num2);
  // }

  // const person: IPerson = {
  //   name: "Stefan",
  //   smdfskdfjkjdfkdsfjdskj: "jsdjfj",
  //   age: 27
  // }

  // console.log(sum(1, 2))

  const [incrementList, setIncrementnList] = useState<number[]>([]);

  const addNewIncrement = (newIncrement: number) => {
    setIncrementnList(prevState => [...prevState, newIncrement])
  }

  console.log(incrementList)

  return (
    <div>
      <CounterInput addNewIncrement={addNewIncrement} />
      <CounterList incrementList={incrementList} />
    </div>
  );
}

export default App;
