import Counter from "./Counter";

interface IProps {
    incrementList: number[]
}

const CounterList: React.FC<IProps> = ({ incrementList }) => {
    return (
        <div>
            {incrementList.map(increment => <Counter incrementValue={increment}/>)}
        </div>
    )
}

export default CounterList;