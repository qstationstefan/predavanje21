import { useState } from "react"

interface IProp {
    incrementValue: number
}

const Counter: React.FC<IProp> = ({ incrementValue }) => {
    const [currentValue, setCurrentValue] = useState<number | null>(null);

    const incrementHandler = () => {
        setCurrentValue(prev => prev === null ? 1 : prev + 1);
    }

    return (
        <div>
            <p>Value: {currentValue === null ? "Nije inicijalizovano" : currentValue}</p>
            <p>Increment value: {incrementValue}</p>
            <button onClick={incrementHandler}>Increment</button>
        </div>
    )
}

export default Counter