import { FormEvent, useState } from "react";

interface IProps {
    addNewIncrement: (newIncrement: number) => void
}

const CounterInput: React.FC<IProps> = (props) => {
    const [value, setValue] = useState("")

    const submitHandler = (e: FormEvent) => {
        e.preventDefault();
        props.addNewIncrement(+value);
    }

    return (
        <form onSubmit={submitHandler}>
            <input value={value} onChange={e => setValue(e.target.value)} />
            <button type="submit">Submit</button>
        </form>
    )
}

export default CounterInput;